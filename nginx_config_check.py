#!/usr/bin/env python
# _*_ coding:utf-8 _*_
#nginx_config_check
#author: zaki
#version:1.0.0
#desc: nginx_config_check
#date: 2016/07/21
import re,commands,os
from sys import argv,exit
try:
    if not argv[1]:
       print "hello"
    if not os.path.isfile("%s" % argv[1]):
        print "%s not exists!" % argv[1]
        exit()
except:
    exit()
pattern_succ=re.compile(r'syntax is ok')
pattern_fail=re.compile(r'\[emerg\].*')
def Cmd(nginxbinpath=""):
    data=commands.getstatusoutput("%s -t" % nginxbinpath)
    return data
    
def main():
    datavalue=Cmd("%s" % argv[1])
    if len(pattern_succ.findall(datavalue[1])) >=1:
        print "ok"
    else:
        print pattern_fail.findall(datavalue[1])[0]

if __name__ == '__main__':
    main()

