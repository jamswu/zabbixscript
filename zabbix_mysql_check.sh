#!/bin/bash
# _*_ coding:utf-8 _*_
# zabbix_mysql_chek.sh
# author:  zaki
# version: 2.0.0
# desc: Zabbix monitoring script
# date: 2019/10/25
##################################

# Zabbix requested parameter
ZBX_REQ_DATA="$1"
ZBX_REQ_DATA_SOURCE="$2"
ZBX_REQ_DATA_PORT="$3"
source /etc/bashrc
# if [[ $1 =~ ^[0-9]\{1,\}$ ]]; then
#   print $2
#   exit 0
portstr="$2"
if [ "$1" == "DiscoveryPort" ];then
  portarr=(${portstr//,/ })
  echo '{"data":['
  tag=1
  for data in ${portarr[@]}
  do
    # if [[ $data =~ ^[0-9]\{1,\}$ ]]; then
      if [ $tag -eq 0 ];then
          echo ","
      fi
       echo -n '{"{#DATABASEPORT}":"'$data'"}'
       tag=0
    # fi
  done
  echo 
  echo ']}'
  exit 0
elif [ "$1" == "DiscoveryPort1" ];then
  portarr=(${portstr//,/ })
  echo '{"data":['
  tag=1
  for data in ${portarr[@]}
  do
    # if [[ $data =~ ^[0-9]\{1,\}$ ]]; then
      if [ $tag -eq 0 ];then
          echo ","
      fi
       echo -n '{"{#DATABASEPORT1}":"'$data'"}'
       tag=0
    # fi
  done
  echo 
  echo ']}'
  exit 0
elif [ "$1" == "DiscoveryPort2" ];then
  portarr=(${portstr//,/ })
  echo '{"data":['
  tag=1
  for data in ${portarr[@]}
  do
    # if [[ $data =~ ^[0-9]\{1,\}$ ]]; then
      if [ $tag -eq 0 ];then
          echo ","
      fi
       echo -n '{"{#DATABASEPORT2}":"'$data'"}'
       tag=0
    # fi
  done
  echo 
  echo ']}'
  exit 0
fi


#用于监控多实例数据库使用
dbusername='monitoring'
dbhost='127.0.0.1'
dbpassword='8e32be1288a33a40'

# MySQL details
MYSQL_ACCESS="/usr/local/shell/zabbixscript/mysql.conf"

if [  -f "/usr/local/webserver/mysql5.6.19/bin/mysql" ];then
    MYSQL_BIN="/usr/local/webserver/mysql5.6.19/bin/mysql" 
elif [ -f  "/usr/local/mysql/bin/mysql" ];then
    MYSQL_BIN="/usr/local/mysql/bin/mysql"
elif [ -f "/usr/local/webserver/mysql/bin/mysql" ];then
    MYSQL_BIN="/usr/local/webserver/mysql/bin/mysql"
else
    MYSQL_BIN=`which mysql`
fi

if [  -f "/usr/local/webserver/mysql5.6.19/bin/mysqladmin" ];then
    MYSQLADMIN_BIN="/usr/local/webserver/mysql5.6.19/bin/mysqladmin" 
elif [ -f  "/usr/local/mysql/bin/mysqladmin" ];then
    MYSQLADMIN_BIN="/usr/local/mysql/bin/mysqladmin"
elif [ -f "/usr/local/webserver/mysql/bin/mysqladmin" ];then
    MYSQLADMIN_BIN="/usr/local/webserver/mysql/bin/mysqladmin"
else
    MYSQLADMIN_BIN=`which mysqladmin`
fi


if [ $ZBX_REQ_DATA_PORT -ge 0 ] 2>/dev/null;then
    MYSQL="$MYSQL_BIN -u$dbusername -p$dbpassword -h $dbhost -P $ZBX_REQ_DATA_PORT"
    MYSQL_SLAVE="$MYSQL_BIN -u$dbusername -p$dbpassword -h $dbhost -P $ZBX_REQ_DATA_PORT"
    MYSQLADMIN="$MYSQL_BIN -u$dbusername -p$dbpassword -h $dbhost -P $ZBX_REQ_DATA_PORT"
else
    MYSQL="$MYSQL_BIN --defaults-extra-file=$MYSQL_ACCESS"
    MYSQL_SLAVE="$MYSQL_BIN --defaults-extra-file=$MYSQL_ACCESS"
    MYSQLADMIN="$MYSQL_BIN --defaults-extra-file=$MYSQL_ACCESS"
    
    echo "" | $MYSQL 2>/dev/null
    if [ $? -ne 0 ]; then
        ZBX_REQ_DATA_PORT=`cat $MYSQL_ACCESS|grep port|awk -F '=' '{print $2}'`
        MYSQL="$MYSQL_BIN -u$dbusername -p$dbpassword -h $dbhost -P $ZBX_REQ_DATA_PORT"
        MYSQL_SLAVE="$MYSQL_BIN -u$dbusername -p$dbpassword -h $dbhost -P $ZBX_REQ_DATA_PORT"
        MYSQLADMIN="$MYSQL_BIN -u$dbusername -p$dbpassword -h $dbhost -P $ZBX_REQ_DATA_PORT"
    fi
fi

# MYSQL_SLAVE="$MYSQL_BIN --defaults-extra-file=$MYSQL_ACCESS"
# MYSQL_SLAVE="$MYSQL_BIN -uroot -pzaki.com -h 127.0.0.1 -P 3306 --show-warnings=false"
#MYSQL="$MYSQL_BIN"
#MYSQL_SLAVE="$MYSQL_BIN"

#
# Error handling:
#  - need to be displayable in Zabbix (avoid NOT_SUPPORTED)
#  - numeric items need to be of type "float" (allow negative + float)
#
ERROR_NO_ACCESS_FILE="-0.9900"
ERROR_NO_ACCESS="-0.9901"
ERROR_WRONG_PARAM="-0.9902"

# No mysql access file to read login info from
if [ ! -f "$MYSQL_ACCESS" ]; then
  echo $ERROR_NO_ACCESS_FILE
  exit 1
fi

# Check MySQL access
echo "" | $MYSQL 2>/dev/null
if [ $? -ne 0 ]; then
  echo $ERROR_NO_ACCESS
  exit 1
fi

# Only ZBX_REQ_DATA is mandatory
# If ZBX_REQ_DATA_SOURCE is not specified, get from mysql global status
if [ -z "$ZBX_REQ_DATA" ]; then
  echo $ERROR_WRONG_PARAM
  exit 1
fi

#默认可以接受一个参数
if [ -z "$ZBX_REQ_DATA_SOURCE" ]; then
  ZBX_REQ_DATA_SOURCE='status'
fi
if [ "$ZBX_REQ_DATA_SOURCE" == "status" ]; then
   ZBX_REQ_DATA_SOURCE='status'
fi

#############
# Data retrieve methods
#############

get_from_status(){
  param=$1
  if [ "$param" == "ping" ];then
    value=$(echo "ping" | $MYSQLADMIN 2>/dev/null |grep -c alive)
    if [ $value -ge 1 ];then
      echo 0
    else
      echo 1
    fi
  else
    value=$(echo "show global status like '$param'" | $MYSQL 2>/dev/null | awk '{print $2}'|grep -v Value)
    [ -z "$value" ] && echo $ERROR_WRONG_PARAM || echo $value
  fi
}

get_from_variables(){
  param=$1
  value=$(echo "show global variables like '$param'" | $MYSQL 2>/dev/null | awk '{print $2}'|grep -v Value)
  [ -z "$value" ] && echo $ERROR_WRONG_PARAM || echo $value
}

get_from_master(){
  param=$1
  value=$(echo "show master status \G" | $MYSQL 2>/dev/null| grep -E "^[ ]*$param:" | awk '{print $2}')
  [ -z "$value" ] && echo $ERROR_WRONG_PARAM || echo $value
}

get_from_slave(){
  param=$1
  value=$(echo "show slave status \G" | $MYSQL_SLAVE 2>/dev/null | grep -E "^[ ]*$param:" |grep -v Warning| awk '{print $2}')
  [ -z "$value" ] && echo $ERROR_WRONG_PARAM || echo $value
}

get_auto_increment_value(){
    param=$1
    if [ $param -ge 0] 2>/dev/null;then
        param=$1
    else
        param=3
    fi
    value=$(echo "SELECT TABLE_NAME FROM information_schema.TABLES WHERE AUTO_INCREMENT >=$param AND AUTO_INCREMENT<=4294967295 ORDER BY AUTO_INCREMENT desc limit 15" | $MYSQL 2>/dev/null|grep -v TABLE_NAME)
    for tablename in  $value
        do 
            value1=$(echo "SELECT TABLE_NAME FROM information_schema.COLUMNS where TABLE_NAME='$tablename' and EXTRA='auto_increment' and COLUMN_TYPE like 'int%';"| $MYSQL 2>/dev/null|grep -v TABLE_NAME)
           if  [ -z $value1 ]; then
                continue
            else
                value2=$(echo "SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE AUTO_INCREMENT >=$param AND AUTO_INCREMENT<=4294967295 and TABLE_NAME='$tablename'" | $MYSQL 2>/dev/null|grep -v AUTO_INCREMENT) 
                [ -z "$value2" ] && echo $ERROR_WRONG_PARAM || echo $value2 && break
           fi
       done
}


get_from_innodb_file(){
  param=$1
  datadir=$($MYSQL  --silent -e "show global variables like 'datadir';" 2>/dev/null | awk '{print $2}')
    if [ -z "$datadir" -o ! -e "$datadir" ]; then echo $ERROR_GENERIC; exit 1; fi
  pid_file=$($MYSQL  --silent -e "show global variables like 'pid_file';" | awk '{print $2}')
    if [ -z "$pid_file" ]; then echo $ERROR_GENERIC; exit 1; fi;
    if sudo /usr/bin/test ! -e "$pid_file" ; then echo $ERROR_GENERIC; exit 1; fi;
  innodb_file=$datadir/innodb_status.$(sudo /bin/cat $pid_file)
    if [ "$innodb_file" == "$datadir/innodb_status." ]; then echo $ERROR_GENERIC; exit 1; fi
  innodb_file_content=$(sudo /bin/cat $innodb_file)
    if [ -z "$innodb_file_content" ]; then echo $ERROR_GENERIC; exit 1; fi

  case $param in
    innodb_row_queries)     echo "$innodb_file_content" | grep 'queries inside InnoDB' | awk '{print $1}';;
    innodb_row_queue)       echo "$innodb_file_content" | grep 'queries inside InnoDB' | awk '{print $5}';;
    history_list_length)    echo "$innodb_file_content" | grep -i 'history list' | awk '{print $4}';;
    *) echo $ERROR_WRONG_PARAM; exit 1;;
  esac
}

# 
# Grab data from mysql for key ZBX_REQ_DATA
#
case $ZBX_REQ_DATA_SOURCE in
    slave)
    	get_from_slave	    "$ZBX_REQ_DATA"
        ;;
    master)	
        get_from_master	  "$ZBX_REQ_DATA"
        ;;
    status)	
        get_from_status	  "$ZBX_REQ_DATA"
        ;;
    variables)
    	get_from_variables	"$ZBX_REQ_DATA"
        ;;
    innodb_file) 
    	get_from_innodb_file "$ZBX_REQ_DATA"
        ;;
    auto_increment)
        get_auto_increment_value "$ZBX_REQ_DATA"
        ;;
    *) 
       echo $ERROR_WRONG_PARAM
    #exit 1
     ;;
esac
